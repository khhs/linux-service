﻿using System;

namespace Model
{
    public class UserProfile
    {
        public int UserProfileId { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }
        public String Firstname { get; set; }
        public String Lastname { get; set; }
        public int ProfileType { get; set; }
        public long TimeCreated { get; set; }

        public UserProfile() { }

        public UserProfile(int userProfileId, String username, String password, String email, String firstname, String lastname, int profileType, long timeCreated)
        {
            UserProfileId = userProfileId;
            Username = username;
            Password = password;
            Email = email;
            Firstname = firstname;
            Lastname = lastname;
            ProfileType = profileType;
            TimeCreated = timeCreated;
        }
    }
}
