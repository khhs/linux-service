﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading.Tasks;
using Model;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace DataSource
{
    public class DbCon
    {
        private MySqlConnection Conn;

        public List<UserProfile> UserProfiles {
            get
            {
                return GetUserProfiles();
            }
        }


        public DbCon(String connectionString)
        {
            Conn = new MySqlConnection(connectionString);
            Conn.Open();
        }


        private List<UserProfile> GetUserProfiles()
        {
            MySqlCommand SqlCommand = new MySqlCommand(@"Select * from userprofile;", Conn);
            MySqlDataAdapter CommandAdapter = new MySqlDataAdapter(SqlCommand);

            DataTable DbTable = new DataTable();
            CommandAdapter.Fill(DbTable);

            return ToUserProfile(DbTable.Select());
        }

        private List<UserProfile> ToUserProfile(DataRow[] queryData)
        {
            List<UserProfile> TmpListProfiles = new List<UserProfile>();

            foreach(DataRow Row in queryData) {
                TmpListProfiles.Add(new UserProfile((int)Row.ItemArray[0], (String)Row.ItemArray[1], (String)Row.ItemArray[2],
                    (String)Row.ItemArray[3], (String)Row.ItemArray[4], (String)Row.ItemArray[5], (int)Row.ItemArray[6],
                    (long)Row.ItemArray[7]));
            }
            return TmpListProfiles;
        }

        public async void Dispose()
        {
            await Conn.CloseAsync();
        }


    }
}
